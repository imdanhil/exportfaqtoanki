from bs4 import BeautifulSoup
import requests
import csv

header = ['q','a']

url = "https://github.com/Swfuse/devops-interview/blob/main/interview.md"

page = BeautifulSoup(requests.get(url).text, "lxml")

list_questions = []


for question in page.findAll('h3'):
    print(question.text)
    try:
        link_anchor = url + question.find('a', class_='anchor').get('href')
        link_for_anki = "<a href='" + link_anchor + "'>" + link_anchor + "</a>"
        print(link_for_anki)
        list_questions.append({'q':question.text,'a':link_for_anki})
    except:
        print("-")
   
    

with open('test4.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = header, delimiter ='|')
    writer.writeheader()
    writer.writerows(list_questions)